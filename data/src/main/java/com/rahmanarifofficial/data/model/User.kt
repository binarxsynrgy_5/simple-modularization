package com.rahmanarifofficial.data.model

data class User(
    var name: String? = null,
    var age: Int? = 0
)
