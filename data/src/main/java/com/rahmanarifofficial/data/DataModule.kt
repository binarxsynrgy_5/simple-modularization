package com.rahmanarifofficial.data

import android.app.Application
import com.rahmanarifofficial.data.repository.UserRepository
import org.koin.dsl.module

object DataModule {
    val Application.dataModule get() = module {

        //DATABASE

        //API SERVICE
        /*single { ApiClient.instance }
        single { ApiHelper(get()) }*/

        //REPOSITORY
        /*single { UserRepository(get()) }*/
    }
}