package com.rahmanarifofficial.simplemodularization

import android.app.Application
import com.rahmanarifofficial.data.DataModule.dataModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.GlobalContext.startKoin

class App : Application() {
    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidContext(this@App)
            modules(listOf(
                dataModule,
            ))
        }

    }
}